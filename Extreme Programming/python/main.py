def sword_decorator(func):
    def wrapper():
        stickman = func()
        splitstickman = stickman.split('\n')
        newstickman = ""
        for i in range(len(splitstickman)):
            for j in range(len(splitstickman[i])+1):
                if i == 0 and j == 0:
                    newstickman += '\\'
                elif i == 1 and j == 1:
                    newstickman += '\\'
                elif i == 2 and j == 2:
                    newstickman += '\\'
                elif i == 3 and j == 2:
                    newstickman += '_'
                elif i == 3 and j == 4:
                    newstickman += '|'
                elif i == 3 and j == 3:
                    newstickman += 'M'
                else:
                    try:
                        newstickman += splitstickman[i][j]
                    except:
                        newstickman += ' '
            newstickman += '\n'
        return newstickman
    return wrapper


def shield_decorator(func):
    def wrapper():
        stickman = func()
        splitstickman = stickman.split('\n')
        newstickman = ""
        for i in range(len(splitstickman)):
            for j in range(12):
                if i == 2 and j == 9:
                    newstickman += '}'
                elif i == 3 and j == 9:
                    newstickman += '|'
                elif i == 3 and j == 10:
                    newstickman += '}'
                elif i == 4 and j == 9:
                    newstickman += '}'
                else:
                    try:
                        newstickman += splitstickman[i][j]
                    except:
                        newstickman += ' '
            newstickman += '\n'
        return newstickman
    return wrapper


@sword_decorator
@shield_decorator
def stickman():
    return open('stickman.txt').read()


if __name__ == "__main__":
    print(stickman())
