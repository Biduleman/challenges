# Puzzle Heroes

## Human Shazam

Un mystérieux medley ce trouve dans ce répertoire, vous devez trouver d'où sont tirées chacune des pièces le configurant.

Écrire les réponses dans ce README!

#### Réponses:

###### Extrait 1
votre réponse: X-Files theme

###### Extrait 2
votre réponse: Halo Theme

###### Extrait 3
votre réponse: LQJR Song

###### Extrait 4
votre réponse: Futurama Theme

###### Extrait 5
votre réponse: 

###### Extrait 6
votre réponse: Ducktales NES - The Moon Theme

###### Extrait 7
votre réponse: Death Note Intro 1

###### Extrait 8
votre réponse: Conker's Bad Fur Day (N64) - The Great Might Poo song

###### Extrait 9
votre réponse: Chuck Intro

###### Extrait 10
votre réponse: Zelda a Link to the Past Overworld

Bonne chance !