# OS

## Super quiz!

### Instruction
Vous devez répondre au questionnaire directement dans ce README en dessous de chaque question. Ces questions touchent plusieurs aspects de la programmation de bas niveau et les réponses sont généralement assez courtes.

###### Question 1
Quelle est la différence entre le heap et le stack (le monceau et la pile).
Le stack est utilisé pour l'allocation de mémoire statique et le heap pour la mémoire dynamique. Le stack est alloué à la compilation, les adresses du heap le sont au runtime

###### Question 2
Que fais la fonction malloc?
Malloc alloue un bloc de mémoire d'une certaine grandeur et retourne le pointeur vers le début du bloc.

###### Question 3
Décrivez le fonctionnement de ce code.

```
uint16_t lastKeys = 0;
uint16_t diff, currentKeys;

while (1) {
	currentKeys = scanKeyboard();
	if ((diff = (uint16_t) (currentKeys & (~lastKeys))) || !currentKeys)
		lastKeys = currentKeys;
	faire_quelque_chose(diff);
}
```

###### Question 4
Que signifie les 8 paramètres de cette commande GCC:  
`gcc -Os -Wall -g3 -flto -lpthread -o main main.c`
-Os:  Optimise le code pour l'espace utilisé. Les optimisations qui réduisent la taille du code ou qui améliore les performances sans grossrir la taille du code.
-Wall: Met en place tous les avertissements de construction
-g3: Produit des informations de debbuging avec de l'information supplémentaire comme les définitions de macro présentes dans le programme
-flto:
-

###### Question 5
À quoi sert le DMA dans un microcontrolleur ou un ordinateur.


###### Question 6
Quelle est la différence entre mutex et sémaphore?


###### Question 7
Un microcontrolleur lit, avec son convertisseur analogique à numérique à 12 bits, la température d'une cuve de fermentation à l'aide d'un thermomètre. Votre microcontrolleur fonctionne en 5V. Le thermomètre retourne une valeur entre 0 et 5V correspondant aux températures respectivement entre 15-25 degrées Celcius. Assumez que le comportement du thermomètre est linéaire.
Donnez la précision de lecture de votre microcontrolleur. Dans quel type de variable devriez-vous sauvegarder la valeur lue de façon à conserver le maximum de précision (char, int, long, float, double, etc), pourquoi? S'il y a lieu, expliquez aussi à quoi correspondent les valeurs enregistrées. (Max 10 lignes)


###### Question 8
Expliquer une façon de faire le "debounce" d'un bouton.


###### Question 9
Comment pourriez-vous faire pour mesurer la capacitance (en Farrad) d'un condensateur à l'aide d'un Arduino ?


###### Question 10
Que fait ce code et à quoi peut-il servir ? Existe-t-il un équivalent dans la librairie standard du C?

`while (*p++ = *q++) ;`


###### Question 11
Décrivez les principales fonctions d'un microprocesseur.
